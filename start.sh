#!/bin/bash

checkcommand() {
	if ! command -v "$1"; then
		echo "$1 not found, please install" 1>&2
		exit 1
	fi
}

install_deps() {
    if command -v pacman && command -v yay
    then
        sudo pacman -Sy --needed --noconfirm tmux tmuxp
        command -v tt++ || yay -S tintin
    fi
}

checkcommand tmux
checkcommand tmuxp
checkcommand tt++
checkcommand sed

[ -e logs ] || mkdir logs
[ -e logs/chat.log ] || touch logs/chat.log
[ -e logs/minimap.log ] || touch logs/minimap.log

tmuxp load session.yaml
